package com.adia.srs.portcheckinservice.consumerdrivencontract.asconsumer;

import com.adia.srs.portcheckinservice.domain.entities.BookingStatus;
import com.adia.srs.portcheckinservice.domain.events.BookingCreatedEvent;
import com.adia.srs.portcheckinservice.domain.events.BookingStatusChangedEvent;
import com.adia.srs.portcheckinservice.gateway.MessagingGateway;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.cloud.contract.stubrunner.StubTrigger;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.NONE)
@AutoConfigureStubRunner(ids = "com.adia.srs:bookingservice:+")
@ActiveProfiles(profiles = "testing")
public class MessagingTests {

    @Autowired
    StubTrigger stubTrigger;

    @SpyBean
    private MessagingGateway messagingGateway;

    @Test
    public void testBookingCreatedCompliesToContract() throws Exception {

        stubTrigger.trigger("testBookingCreated");

        ArgumentCaptor<BookingCreatedEvent> argument = ArgumentCaptor.forClass(BookingCreatedEvent.class);
        verify(messagingGateway).handle(argument.capture());
        assertThat(argument.getValue().getBookingId()).isEqualTo(1L);
    }

    @Test
    public void testBookingStatusChangedCompliesToContract() throws Exception {

        stubTrigger.trigger("testBookingStatusChanged");

        ArgumentCaptor<BookingStatusChangedEvent> argument = ArgumentCaptor.forClass(BookingStatusChangedEvent.class);
        verify(messagingGateway).handle(argument.capture());
        assertThat(argument.getValue().getBookingId()).isEqualTo(1L);
        assertThat(argument.getValue().getOldStatus()).isEqualTo(BookingStatus.REQUESTED);
        assertThat(argument.getValue().getNewStatus()).isEqualTo(BookingStatus.ACCEPTED);
    }
}

