package com.adia.srs.portcheckinservice.consumerdrivencontract.asproducer;

import com.adia.srs.portcheckinservice.Application;
import com.adia.srs.portcheckinservice.domain.entities.CheckInStatus;
import com.adia.srs.portcheckinservice.domain.events.CheckInStatusChangedEvent;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.NONE)
@AutoConfigureMessageVerifier
@ActiveProfiles(profiles = "testing")
public abstract class MessagingTestsBase {

    @Autowired
    private Source source;

    protected void testCheckIn() {
        source.output().send(MessageBuilder
                .withPayload(new CheckInStatusChangedEvent(1L, CheckInStatus.CHECKED_IN))
                .setHeader("type", CheckInStatusChangedEvent.class.getSimpleName())
                .build());
    }

    protected void testCheckOut() {
        source.output().send(MessageBuilder
                .withPayload(new CheckInStatusChangedEvent(2L, CheckInStatus.CHECKED_OUT))
                .setHeader("type", CheckInStatusChangedEvent.class.getSimpleName())
                .build());
    }
}