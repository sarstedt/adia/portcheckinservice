package com.adia.srs.portcheckinservice.consumerdrivencontract.asconsumer;

import com.adia.srs.portcheckinservice.domain.entities.Customer;
import com.adia.srs.portcheckinservice.gateway.BookingServiceGateway;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureStubRunner(ids = "com.adia.srs:bookingservice:+")
@ActiveProfiles(profiles = "testing")
public class RestTests {

    @StubRunnerPort("bookingservice")
    private int bookingservicePort;

    @Autowired
    private BookingServiceGateway bookingServiceGateway;

    @Before
    public void setUp() {

        bookingServiceGateway.setBookingserviceURL("http://localhost:" + bookingservicePort + "/v1/customers");
    }

    @Test
    public void getCustomersCompliesToContract() {
        List<Customer> customers = bookingServiceGateway.getCustomers();
        assertThat(customers).isNotEmpty();
    }
}
