package com.adia.srs.portcheckinservice.gateway;

import com.adia.srs.portcheckinservice.domain.entities.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class BookingServiceGateway {

    private final RestTemplate restTemplate;
    private String bookingserviceURL;

    @Autowired
    public BookingServiceGateway(RestTemplateBuilder restTemplateBuilder,
                                 @Value("${BOOKINGSERVICE_URL:http://bookingservice/v1/customers}") String bookingserviceURL) {
        this.bookingserviceURL = bookingserviceURL;
        this.restTemplate = restTemplateBuilder.build();
    }

    public void setBookingserviceURL(String bookingserviceURL) {
        this.bookingserviceURL = bookingserviceURL;
    }

    public List<Customer> getCustomers() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        ResponseEntity<List<Customer>> response =
                restTemplate.exchange(bookingserviceURL, HttpMethod.GET,
                        new HttpEntity<>(httpHeaders),
                        new ParameterizedTypeReference<List<Customer>>() {
                        });
        return response.getBody();
    }
}