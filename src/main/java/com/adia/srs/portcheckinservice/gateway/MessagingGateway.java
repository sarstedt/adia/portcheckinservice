package com.adia.srs.portcheckinservice.gateway;

import com.adia.srs.portcheckinservice.domain.events.BookingCreatedEvent;
import com.adia.srs.portcheckinservice.domain.events.BookingStatusChangedEvent;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

/**
 * Implements the Messaging Gateway Pattern
 *
 * @see <a href="https://www.enterpriseintegrationpatterns.com/patterns/messaging/MessagingGateway.html">Messaging Gateway Pattern</a>
 */
@Service
public class MessagingGateway {

    private final Log log = LogFactory.getLog(getClass());

    @Autowired
    private Source source;

    @StreamListener(
            target = Processor.INPUT,
            condition = "headers['type']=='BookingCreatedEvent'")
    public void handle(BookingCreatedEvent bookingCreatedEvent) {

        log.info("Received new verification, bookingId=" + bookingCreatedEvent.getBookingId());
    }

    @StreamListener(
            target = Processor.INPUT,
            condition = "headers['type']=='BookingStatusChangedEvent'")
    public void handle(BookingStatusChangedEvent bookingStatusChangedEvent) {

        log.info("Received new verification, bookingId=" + bookingStatusChangedEvent.getBookingId());
    }

    public <T> boolean publish(T message) {
        return source.output().send(MessageBuilder
                .withPayload(message)
                .setHeader("type", message.getClass().getSimpleName())
                .build());
    }
}
