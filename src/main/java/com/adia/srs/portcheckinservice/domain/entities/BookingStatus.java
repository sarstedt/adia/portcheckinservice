package com.adia.srs.portcheckinservice.domain.entities;

public enum BookingStatus {
    REQUESTED, ACCEPTED(REQUESTED), DECLINED(REQUESTED), CANCELED(REQUESTED), AWAITING_PAYMENT(ACCEPTED), CONFIRMED(AWAITING_PAYMENT), CLOSED(CONFIRMED);

    private BookingStatus[] previousStates;

    private BookingStatus(BookingStatus... state) {
        this.previousStates = state;
    }


    public BookingStatus transition(BookingStatus newState) {
        for (BookingStatus previous : newState.previousStates) {
            if (this == previous) {
                return newState;
            }
        }
        throw new IllegalArgumentException(String.format("Illegal state transition from %s to %s.", this.toString(), newState.toString()));
    }
}
