package com.adia.srs.portcheckinservice.domain.events;

import com.adia.srs.portcheckinservice.domain.entities.BookingStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookingStatusChangedEvent {

    private Long bookingId;

    private BookingStatus oldStatus;

    private BookingStatus newStatus;
}
