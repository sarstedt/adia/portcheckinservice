package com.adia.srs.portcheckinservice.domain.entities;

public enum Gender {
    MALE, FEMALE, OTHER, UNKNOWN
}
