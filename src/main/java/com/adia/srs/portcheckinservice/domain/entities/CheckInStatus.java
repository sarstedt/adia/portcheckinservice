package com.adia.srs.portcheckinservice.domain.entities;

public enum CheckInStatus {
    PRE_CHECKIN, CHECKED_IN(PRE_CHECKIN), CHECKED_OUT(CHECKED_IN);

    private CheckInStatus[] previousStates;

    private CheckInStatus(CheckInStatus... state) {
        this.previousStates = state;
    }

    public CheckInStatus transition(CheckInStatus newState) {
        for (CheckInStatus previous : newState.previousStates) {
            if (this == previous) {
                return newState;
            }
        }
        throw new IllegalArgumentException(String.format("Illegal state transition from %s to %s.", this.toString(), newState.toString()));
    }
}
