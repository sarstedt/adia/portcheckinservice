#!/usr/bin/env bash

if [ "$#" -ne 1 ]; then
    echo "usage: ./`basename "$0"` <new package name>"
    echo "example: ./`basename "$0"` hanna"
    exit 1
fi

NEW_PACKAGE=$1
CURRENT_PACKAGE=`ls ./src/main/java/com/adia/`

echo "Package \"$CURRENT_PACKAGE\" will be renamed to \"$NEW_PACKAGE\"."
echo "WARNING: This will change contents of your project files and directory-paths!"
echo "Type \"yes\" to continue."
read continue

if [ "$continue" != "yes" ]; then
    echo "Aborting."
    exit 1
fi

echo -n "Renaming packages in all source- and test-files..."
find ./src -type f -print0 | xargs -0 sed -i'' "s/com\.adia\.$CURRENT_PACKAGE/com.adia.$NEW_PACKAGE/g"
echo "done."

echo -n "Substituting project package configuration in settings.gradle..."
sed -i'' "s/com\.adia\.$CURRENT_PACKAGE/com.adia.$NEW_PACKAGE/g"  settings.gradle
echo "done."

echo -n "Renaming source directories..."
mv ./src/main/java/com/adia/$CURRENT_PACKAGE ./src/main/java/com/adia/$NEW_PACKAGE
mv ./src/test/java/com/adia/$CURRENT_PACKAGE ./src/test/java/com/adia/$NEW_PACKAGE
echo "done."

echo "Project configuration finished!"

